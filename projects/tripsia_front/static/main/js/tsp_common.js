$(document).ready(function () {

    // **********
    // ******* fixed menu ********
    // *********
    var fixedMenu = function () {
        if ($(window).width() > 767) {
            if ($(window).scrollTop() > 50) {
                $('.header-top-line').addClass('active')
            } else {
                $('.header-top-line').removeClass('active')
            }
        }
    };
    $(window).on('scroll', function () {
        fixedMenu()
    });
    fixedMenu(); //fixed menu


    // **********
    // ******* WoW js ********
    // *********
    new WOW().init();  // wow js

    // **********
    // ******* collapse nav site ********
    // *********
    $('.collapse-btn').on('click', function () {
        $(this).closest('.collapse-box').find('.collapse-desc').slideToggle(400);
        $(this).toggleClass('active')
    });

    $('.burger, .burger *').on('click', function () {
        var nav = $('.main-nav');
        nav.slideToggle();
        nav.css('display', 'flex');
        $(this).toggleClass('open');
    });    // collapse nav site

    // **********
    // ******* scrolling function ********
    // *********
    $('a.scrollto').click(function () {
            var elementClick = $(this).attr("href");
            var destination = $(elementClick).offset().top - 80;
            $("html, body").animate({
                scrollTop: destination
            }, 800);
        }
    );    // end scrolling function

    // **********
    // ******* modal window actions ********
    // *********
    var modalOpen = function () {
        $('#modal').fadeIn()
    };

    $('.closer-modal').on('click', function () {
        $(this).closest('#modal').fadeOut()
    }); // modal window actions

    // **********
    // ******* form actions ********
    // *********
    $('.form-window-action').on('click', function () {
        var input = $(this).closest('.form-window').find('input');
        if (input.val().length < 6) {
            input.removeClass('good');
            input.addClass('invalid shake animated');
            $('.text-weak').removeClass('weak-p-none');
            $('.weak-ckaract').removeClass('weak-p-none');
            $('.text-good').addClass('good-p-none');
            $('.weak-none').addClass('weak-active');
        } else {
            input.removeClass('invalid');
            input.addClass('good');
            $('.weak-ckaract').addClass('weak-p-none');
            $('.text-weak').addClass('weak-p-none');
            $('.text-good').removeClass('good-p-none');
            modalOpen();
        }
    }); // end form actions

    $(document).on('click', '.form-window-modal', function () {
        modalOpen();
    });

    // **********
// ******* walletKey actions ********
// *********
    $(document).on('input', '.walletKey', function () {
        var val = $(this).val();
        if (val.length < 6) {
            $(this).removeClass('good');
            $(this).addClass('invalid shake animated');
            $('.text-weak').removeClass('weak-p-none');
            $('.weak-ckaract').removeClass('weak-p-none');
            $('.text-good').addClass('good-p-none');
            $('.weak-none').addClass('weak-active');
            $('.form-window-links').addClass('hidden')
            $('.btn-create').removeClass('success-button').addClass('disable-btn')
        } else if (val.length === 0) {
            $('.weak-none').removeClass('weak-active');
        } else {
            $(this).removeClass('invalid');
            $(this).addClass('good');
            $('.weak-ckaract').addClass('weak-p-none');
            $('.text-weak').addClass('weak-p-none');
            $('.text-good').removeClass('good-p-none');
            $('.btn-create').addClass('success-button').removeClass('disable-btn')
        }
    }); // end walletkey

    $(document).on('click', '.btn-create.success-button', function () {
        window.location.href = $(this).data('href');
    });

    // **********
    // ******* window resize function ********
    // *********
    $(window).on('resize', function () {
        fixedMenu()
    }); // end resize functions

    // **********
    // ******* language select function ********
    // *********
    $('.lang-opener').on('click', function () {
        $('.lang').toggleClass('open');
        $(this).toggleClass('active');
    }); // end language select function

    // **********
    // ******* modal opener ********
    // *********
    $(document).on('click', '.modal-opener', function () {
        $('.cabinet-select-inf-text-error').css('opacity', 0);
        $('.changeButton').removeClass('repeat').addClass('success-button');
        $('.access-btn').removeClass('success-button').addClass('disable-btn');
        if ($('#access')) {
            $('#access').prop('checked', false)
        }
        var id = $(this).data('id');
        $(id).fadeIn()
    });    // end modal opener

    // **********
    // ******* modal closer ********
    // *********
    $(document).on('click', '.access-modal-close-btn', function () {
        $(this).closest('.modal-wrap').fadeOut()
    });// end modal closer


    // **********
    // ******* modal input edit ********
    // *********
    $(document).on('input', '.inputEdit', function (e) {
        var val = $(this).val();
        var successButton = $(this).closest('.modal-wrap').find('.changeButton');
        if (val.length >= 2) {
            $('.error-txt').css('opacity', 0);
            successButton.removeClass('disable-btn').addClass('success-button').unbind('click')
        } else {
            successButton.removeClass('success-button').addClass('disable-btn')
            successButton.on('click', function (e) {
                e.preventDefault()
            })
        }
    });// end modal closer
    // Modal success button 1
    $(document).on('click', '.changeButton.success-button', function (e) {
        e.preventDefault();
        $(this).removeClass('success-button checking').addClass('repeat disable-btn');
        $('.error-txt').css('opacity', 1);
        $('.repeat').addClass('rotate');
        setTimeout(function () {
            $('.repeat').removeClass('rotate');
        }, 1000)
    });
    // Modal repeat button
    $(document).on('click', '.repeat.disable-btn', function (e) {
        e.preventDefault()
    });
    // Modal repeat success
    $(document).on('click', '.repeat.success-button', function (e) {
        window.location.href = $(this).attr('href');
    });
    // **********
    // ******* input actions ********
    // *********

    $('.checking').click(function (e) {
        e.preventDefault()
    });

    // Send and Check status | CABINET page
    $(document).on('click', '.cabinet-middle-box-head-title', function () {
        $('.cabinet-middle-box-head-title').addClass('box-title-disable');
        $(this).removeClass('box-title-disable');
        $('.cabinet-middle-box-btm').addClass('hidden');
        $('#' + $(this).data('id')).removeClass('hidden');
    });


    $(document).on('input', '#access', function () {
        var btn = $(this).closest('.access-modal-box').find('.checking');
        if ($(this).is(":checked")) {
            btn.removeClass('disable-btn').addClass('success-button');
            btn.unbind('click');
        } else {
            btn.addClass('disable-btn').removeClass('success-button');
            btn.on('click', function (e) {
                e.preventDefault()
            });
        }
        /*input actions*/
    });


    // **********
    // ******* account  wrap ********
    // *********
    var isEnter = false;
    $(document).on('click', '.account-wrap-btn', function (e) {
        var href = $(this).data('href');
        e.preventDefault();
        if (!isEnter) {
            $(this).closest('.account').find('.error-col').addClass('show');
            $(this).removeClass('success-button').addClass('disable-btn');
            $(document).on('input', '.account-wrap-user', function () {
                $('.account-wrap-btn').addClass('success-button btn-create');
                isEnter = true;
            });
        } else {
            window.location.href = href
        }
    });


    // account  wrap


    // **********
    // ******* refresh button action ********
    // *********
    $(document).on('click', '.refresh-btn', function () {
        $('.cabinet-head-box-refresh-text').css('opacity', 1);
        $(this).css('opacity', '.5')
        setTimeout(function () {
            $('.refresh-btn').css('opacity', '1')
        }, 500);
    });    // refresh button action

    // **********
    // ******* search action ********
    // *********
    $(document).on('click', '#search', function () {
        var input = $('#searchInput');
        if (input.val() !== '') {
            $('.finding-element').fadeIn();
        } else {
            $('.finding-element').fadeOut();
        }
    });   // search action


    // **********
    // ******* print and download action ********
    // *********
    $(document).on('click', '#printWindow', function () {
        var printWindow = window.open(
            '/print_private_key',
            'window',
            'width = 350, height = 600, toolbar = 0, location = 0'
        );
    });
    $(document).on('click', '#printWindowRU', function () {
        var printWindow = window.open(
            'privatekeyRU.html',
            'window',
            'width = 350, height = 600, toolbar = 0, location = 0'
        );
    });

    $(document).on('click', '#printPageBtn', function () {
        window.open('/print_private_key').print()
    }); // print and download action

    // **********
    // ******* copy text action ********
    // *********
    $(document).on('click', '.qr-code-modal-action-btn', function () {
        var copiedText = $('.text-copied'),
            copyText = $('.copy-text').html(),
            tempElement = $('<input>').val(copyText).appendTo('body').select();

        document.execCommand('copy');
        tempElement.remove();
        $(this).removeClass('success-button').addClass('disable-btn');
        copiedText.css('opacity', '1');
    });

    $(document).on('click', '.copy-list-text', function () {
        var textCopied = $('.copy-text-out-modal');
        copyText = $('.copy-text').html(),
            tempElement = $('<input>').val(copyText).appendTo('body').select();

        document.execCommand('copy');
        tempElement.remove();
        $(this).removeClass('cur-p').css('opacity', '0.5');
        setTimeout(function () {
            $('.copy-list-text').css('opacity', '1')
        }, 500);
        textCopied.css('opacity', 1);
    });    //copy text action

    // **********
    // ******* user auth action ********
    // *********
    $(document).on('click', '.auth', function () {
        $(this).toggleClass('active');
    });// user auth action

    // **********
    // ******* cloning news action ********
    // *********
    $(document).on('click', '.news-box-more-btn', function () {
        var newsBox = $('.news-wrap'),
            newsWrap = $('.main-page-news');
        newsBox.clone().appendTo(newsWrap);
    });    // cloning news action

    // **********
    // ******* showHandlerTextInform ********
    // *********
    $(document).on('click', '.showHandlerTextInform', function (e) {
        e.preventDefault();
        var id = $(this).data('id');
        $(id).css('opacity', 1)
    });    // showHandlerTextInform


    // **********
    // ******* main carousel ********
    // *********
    $('.header-middle-carousel').owlCarousel({
        items: 1,
        autoplay: true,
        autoplayTimeout: 3500,
        autoplayHoverPause: true,
        smartSpeed: 700,
        loop: true
    });
    // main carousel

    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 50) {
            $('.sticky .nav-brand img').attr('src', '../../static/main/BWW_IMAGES/group-9@2x.png')
        } else {
            $('.header-area .nav-brand img').attr('src', '../../static/main/BWW_IMAGES/group-9@2x.png');
            $('.in-inner-page .nav-brand img').attr('src', '../../static/main/BWW_IMAGES/group-9@2x.png');
        }
    });

    // **********
    // ******* showing password ********
    // *********
    var showingPassword = false;
    $(document).on('click', '.showing-password', function () {
        showingPassword = !showingPassword;
        var type = showingPassword ? 'text' : 'password';
        $(this).parent().find('input').attr('type', type)
    });    //showing password


    // **********
    // ******* checked checkbox ********
    // *********
    $(document).on('input', '.account-check-wrap-top input', function () {
        if ($(this).is(':checked')) {
            $('.account-check-wrap-bottom input').prop('checked', true);
        } else if (!$(this).is(':checked')) {
            $('.account-check-wrap-bottom input').prop('checked', false);
        }
    });    //checked checkbox


    // **********
    // ******* transaction action ********
    // *********
    $(document).on('click', '.transaction-btn', function (e) {
        e.preventDefault();
        $('.transaction-window').slideToggle()
    });    //transaction action

    // **********
    // ******* file upload button ********
    // *********
    $(document).on('click', '.file-btn', function () {
        $('#file').trigger("click");
    });


    $('.transaction-butt').click(function (e) {
        e.preventDefault()
    });

    $('.disable-input').on('input', function () {
        var dbButton = $('.transaction-butt');
        if($(this).val().length > 0) {
            dbButton.addClass('success-button transaction-btn').removeClass('disable-btn');
        } else {
            dbButton.removeClass('success-button transaction-btn').addClass('disable-btn');
            $('.transaction-window').slideUp()

        }

    })


});
