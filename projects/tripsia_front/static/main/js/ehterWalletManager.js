var selectedFile;

var production = "http://167.99.226.94";
var development = "http://127.0.0.1:8000";
var api = development;
var api_tsp = "http://127.0.0.1:8000/tsp";


var tspApiType = 10001;
var tstApiType = 10002;


function createWallet() {

    var walletPassword = $('#walletPassword').val();


    console.log(walletPassword);

    var dataToSend = {
        'password': walletPassword
    };

    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: '/api/v1/wallet/create/',
        async: true,
        data: JSON.stringify(dataToSend),
        dataType: 'json',
        success: function (result) {
            if (result['status'] === 'success') {
                console.log(result);
                // window.location.href = "http://127.0.0.1:8000/keystore";
                window.location.href = api + "/keystore";
                //downloadKeystore(result['keystore']);
                // downloadKeystore();

                //
                //          $('#mainContent').html('');
                //          var html = " <main class=\" main-page \">\n" +
                //         "    <script src=\"{% static 'main/js/jquery.min.js' %}\"></script>\n" +
                //         "<!-- Popper js -->\n" +
                //         "<script src=\"{% static 'main/js/popper.min.js' %}\"></script>\n" +
                //         "<!-- Bootstrap js -->\n" +
                //         "<script src=\"{% static 'main/js/bootstrap.min.js' %}\"></script>\n" +
                //         "<!-- All Plugins js -->\n" +
                //         "<script src=\"{% static 'main/js/plugins.js' %}\"></script>\n" +
                //         "<!-- Parallax js -->\n" +
                //         "<script src=\"{% static 'main/js/dzsparallaxer.js' %}\"></script>\n" +
                //         "\n" +
                //         "<script src=\"{% static 'main/js/jquery.syotimer.min.js' %}\"></script>\n" +
                //         "\n" +
                //         "<script src=\"{% static 'main/js/common.js' %}\"></script>\n" +
                //         "\n" +
                //         "<!-- script js -->\n" +
                //         "<script src=\"{% static 'main/js/script.js' %}\"></script>\n" +
                //         "<script src=\"{% static 'main/js/base.js' %}\"></script>" +
                //         "" +
                //         "<form action=\" \" onsubmit=\"return false;\">\n" +
                //         "        <div class=\"container\" style=\"padding-bottom: 90px;\">\n" +
                //         "      <div class=\"row justify-content-center text-center\">\n" +
                //         "        <div class=\"col-lg-11\">\n" +
                //         "          <div class=\"form-window\">\n" +
                //         "            <h2 class=\"form-window-title f-semi-2\">By Keystore File\n" +
                //         "              <br>\n" +
                //         "              <span class=\"form-window-txt text-center f-16 f-black-2\">Already have a wallet? <a href=\"{% url 'walletaccess' %}\" class=\"f-semi  link\">Access My Wallet</a></span>\n" +
                //         "            </h2>\n" +
                //         "            <div class=\"cont\" id=\"downloadKeystoreContent\"></div>\n" +
                //         "            <div class=\"warning-txt-box\">\n" +
                //         "              <p><span class=\"error-col\">**Do not lose it!**  </span>It cannot be recovered if you lose it.</p>\n" +
                //         "              <p><span class=\"error-col\">**Do not share it!**  </span>Your funds will be stolen if you use this file on\n" +
                //         "                a\n" +
                //         "                malicious/phishing site.</p>\n" +
                //         "              <p><span class=\"error-col\">**Make a backup!**  </span>Secure it like the millions of dollars it may one\n" +
                //         "                day\n" +
                //         "                be worth.</p>\n" +
                //         "            </div>\n" +
                //         "\n" +
                //         "            <button class=\"success-button lh-50 form-window-modal\">I understand. Continue.</button>\n" +
                //         "\n" +
                //         "            <p class=\"form-window-txt\"></p>\n" +
                //         "          </div>\n" +
                //         "        </div>\n" +
                //         "      </div>\n" +
                //         "    </div>\n" +
                //         "    </form>\n" +
                //         "</main>"
                //      $('#mainContent').html(html);
                //      downloadKeystore();

            }
        },
        error: function (error) {
            console.log(error);
        }
    });


}

function downloadKeystore() {
    var dataToSend = "fsf";
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: '/api/v1/wallet/getKeystore/',
        async: true,
        data: JSON.stringify(dataToSend),
        dataType: 'json',
        success: function (result) {
            if (result['status'] === 'success') {
                var keystore = result['keystore'];
                var data = "text/json;charset=utf-8," + encodeURIComponent(keystore);
                var html = '<a class="success-button mb-25 " style="display:inline-flex; align-items: center; margin-top: -10px" href="data:' + data + '" download="keystore.json">Download Keystore File ( JSON )</a>';
                $('div.cont').html(html);
            }
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function importWallet(type) {
    var password = $('#walletPassword').val();
    if (selectedFile !== undefined) {
        var data = {
            'password': password
        };
        var formData = new FormData();
        formData.append('file', selectedFile);
        const data_ = JSON.stringify(data);
        formData.append('data', data_);

        $.ajax({
            type: 'POST',
            cache: false,
            processData: false,
            contentType: false,
            url: '/api/v1/wallet/import/',
            data: formData,
            success: function (result) {
                if (result['status'] === 'success') {
                    console.log(type);
                    if (type === 1001) {
                        window.location.href = api + "/cabinet";
                    } else if (type === 1002) {
                        window.location.href = api + "/cabinet/";
                    } else if (type === 1003) {
                        window.location.href = "http://token.tripsia.io/exchangeGec2/";
                    }
                    else if (type === 1004) {
                        window.location.href = "http://token.tripsia.io/buyTSTUI/";
                    } else if (type === 1005) {
                        window.location.href = api_tsp + "/cabinetA/";
                    }

                } else if (result['status'] === 'error') {
                    var message = result['message'];
                    $('#errorMessage').text(message);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });


    } else {
        console.log("Please select file")
    }

}

function checkBrowserIsSupportFileSelect() {
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        // Read files
    } else {
        alert('The File APIs are not fully supported by your browser.');
    }
}

function checkTxHash() {
    var txHash = $('#txHashValue').val();

    console.log(txHash);

    var dataToSend = {
        'address': txHash
    };

    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: '/api/v1/wallet/txHash/',
        async: true,
        data: JSON.stringify(dataToSend),
        dataType: 'json',
        success: function (result) {
            if (result['status'] === 'success') {
                // $('#txHashContainer').css("visibility", "visible");
                // $('#errorMessage').css("visibility", "hidden");
                var txHash = result['tx_hash'];
                var from = result['from'];
                var to = result['to'];
                var value = result['value'];
                var nonce = result['nonce'];
                var gasLimit = result['gas_limit'];
                var gasPrice = result['gas_price'];

                $('#txHashLabel').text(txHash);
                $('#fromLabel').text(from);
                $('#toLabel').text(to);
                $('#amountLabel').text(value + " ETH");
                $('#nonceLabel').text(nonce);
                $('#limitLabel').text(gasLimit);
                $('#priceLabel').text(gasPrice);
                $('#viewInEtherScan').attr("href", "https://ropsten.etherscan.io/tx/" + txHash)


            } else if (result['status'] === 'error') {
                // $('#txHashContainer').css("visibility", "hidden");
                // $('#errorMessage').css("visibility", "visible");
                // $('#errorMessageLabel').text(result['message']);
                //
                // var arry = [
                //     {'id': 21, 'address': '0xfabaed45780cc6b3c644b47ab368e57c0a66299e', 'recommendor_id': 20},
                //     {'id': 22, 'address': '0xeedcc14f74f35591f882aa3cd262b3d98f06ea03', 'recommendor_id': 20},
                //     {'id': 26, 'address': '0xb974989f28e2e53af9a3a7e10d85b0b3bb578722', 'recommendor_id': 22}];
                //
                // var r = convert(arry);
                // console.log(JSON.stringify(r));
            }
        },
        error: function (error) {
            console.log(error);
        }
    });


}

function convert(array) {
    var map = {};
    for (var i = 0; i < array.length; i++) {
        var obj = array[i];

        if (!(obj.id in map)) {
            map[obj.id] = obj;
            map[obj.id].id = obj.id;
            map[obj.id].text = obj.text;
            map[obj.id].items = []
        }


        // if (typeof map[obj.id].text == 'undefined') {
        //     map[obj.id].id = obj.id;
        //     map[obj.id].text = obj.text;
        //     map[obj.id].recommendor_id = obj.recommendor_id
        // }

        var parent = obj.recommendor_id || '-';
        if (!(parent in map)) {
            map[parent] = {};
            map[parent].items = []
        }

        map[parent].items.push(map[obj.id])

    }
    return map;
}

function alertFunction() {
    alert('ok');
}


window.onload = function () {
    checkBrowserIsSupportFileSelect();

    document.getElementById("keystoreFile").addEventListener("change", function () {
        var file = this.files[0];
        selectedFile = file;
        if (file) {
            var reader = new FileReader();
            reader.onload = function (evt) {
                console.log(evt);
            };
            reader.onerror = function (evt) {
                console.error("An error ocurred reading the file", evt);
            };
            reader.readAsText(file, "UTF-8");

            var fileName = $('#keystoreFile').val();

            $("#selectedFile").text(file.name);

            console.log(reader)
        }
    }, false);

    document.getElementById("keystoreFile_gec2").addEventListener("change", function () {
        var file = this.files[0];
        selectedFile = file;
        if (file) {
            var reader = new FileReader();
            reader.onload = function (evt) {
                console.log(evt);
            };
            reader.onerror = function (evt) {
                console.error("An error ocurred reading the file", evt);
            };
            reader.readAsText(file, "UTF-8");

            var fileName = $('#keystoreFile_gec2').val();

            $("#selectedFile").text(file.name);

            console.log(reader)
        }
    }, false);
    document.getElementById("tsp_balance").addEventListener("load", function () {
        alertFunction();
    });


};


function importWalletByPrivateKey(apiType) {
    var privateKey = $('#privateKey').val();
    var dataToSend = {
        'privateKey': privateKey
    };

    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: '/api/v1/wallet/importPk/',
        async: true,
        data: JSON.stringify(dataToSend),
        dataType: 'json',
        success: function (result) {
            if (result['status'] === 'success') {
                if (apiType === tspApiType) {
                    window.location.href = api_tsp + "/cabinetA/";
                } else if (apiType === tstApiType) {
                    window.location.href = api + "/cabinet/";
                }
            } else if (result['status'] === 'error') {
                var message = result['message'];
                $('#errorMessage').text(message);

            }
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function connectToMetaMask(apiType) {
    if (window.ethereum) {
        window.web3 = new Web3(ethereum);
        try {
            // Request account access if needed
            ethereum.enable();

            console.log("WORRKINGGG lkasdlaksdjlask");
            console.log(apiType);

            var myWeb3 = new Web3(window.web3.currentProvider);

            if (myWeb3.eth.accounts[0]) {
                console.log(myWeb3.eth.accounts[0]);

                var dataToSend = {
                    'publicKey': myWeb3.eth.accounts[0]
                };

                $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url: '/api/v1/wallet/importByMetamask/',
                    async: true,
                    data: JSON.stringify(dataToSend),
                    dataType: 'json',
                    success: function (result) {
                        if (result['status'] === 'success') {
                            if (apiType === tspApiType) {
                                window.location.href = api_tsp + "/cabinetA/";
                            } else if (apiType === tstApiType) {
                                window.location.href = api_tsp + "/cabinetA/";
                            }
                        } else if (result['status'] === 'error') {
                            var message = result['message'];
                            $('#errorMessage').text(message);

                        }
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

        } catch (error) {
            console.log("DENYING");
            console.log(error)
            // User denied account access...
        }
    } else if (window.web3) {
        window.web3 = new Web3(web3.currentProvider);

        console.log("WORRKINGGG");

    }
    // Non-dapp browsers...
    else {
        console.log('Non-Ethereum browser detected. You should consider trying MetaMask!');
    }

}

function waitForReceipt(hash, walletId, cb) {
    var web3Infura = new Web3("https://ropsten.infura.io/v3/eb4c55b5263f4ab8b2c8c8934f422930");
    web3Infura.eth.getTransactionReceipt(hash, function (err, receipt) {
        if (err) {
            error(err);
            console.log("Error : " + err);
        }

        if (receipt !== null) {
            // Transaction went through
            if (cb) {
                var data = {
                    'receipt': receipt,
                    'walletId': walletId
                };
                cb(data);
            }
        } else {
            // Try again in 1 second
            window.setTimeout(function () {
                console.log("Attempting ");
                waitForReceipt(hash, walletId, cb);
            }, 1000);
        }
    });
}

function CheckBalance() {
    var email = $("#user_email").text();
    var hideText = $(this).parent().find('.cabinet-head-box-refresh-text');
    var publicKey = $("#wallet_adress").text();
    var dataToSend = {
        'publicKey': publicKey,
        'email': email
    };
    console.log(dataToSend);
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        // url: '/api/v1/user/accountBalance/',
        url: '/api/v1/user/accountBalance/',
        async: true,
        data: JSON.stringify(dataToSend),
        dataType: 'json',
        success: function (result) {
            if (result["status"] === "success") {
                var data = result['data'];
                console.log(data);
                var tst1_count = data['tst1_count'];
                var tst_count = data['tst_count'];
                // var etherValue = data['etherBalance'];
                var tst = "TST: ";
                var tst1 = "TST1: ";
                $('#tst1Value').val(tst1 + tst1_count);
                $('#tstValue').val(tst + tst_count);
                // $('#etherBalance').text(etherValue);

                hideText.css('opacity', 1)
            } else {
                alertify.error(result["error"]);
            }
        }
    });
}


function getTransactionHistory(txType) {
    var publicKey = $("#wallet_adress").text();
    // var publicKey = "0xda4bb5e8dee99e2aba390372abf880653c774661";

    var dataToSend = {
        'publicKey': publicKey,
        'type': txType,
        'date': 'all'
    };
    console.log(dataToSend);
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: '/api/v1/tst/tstHistoryNew/',
        async: true,
        data: JSON.stringify(dataToSend),
        dataType: 'json',
        success: function (result) {
            if (result["status"] === "success") {
                var data = result['data'];
                console.log(data);

                if (txType === 1001) {
                    var tst1Table = $("#tst1TBody");
                    tst1Table.empty();
                    $.each(data, function (idx, elem) {

                        var newDate = new Date();
                        newDate.setTime(elem.created_at * 1000);
                        var dateString = newDate.toUTCString();

                        tst1Table.append("<tr><th>" + elem.address + "</th><th>" + elem.tst_value + "</th>   <th>" + elem.type + "</th>  <th>" + dateString + "</th></tr>");
                    })

                } else {
                    var tst2Table = $("#tst2Body");
                    tst2Table.empty();


                    $.each(data, function (idx, elem) {
                        var newDate = new Date();
                        newDate.setTime(elem.created_at * 1000);
                        var dateString = newDate.toUTCString();
                        tst2Table.append("<tr><th>" + elem.address + "</th><th>" + elem.tst1_value + "</th>   <th>" + elem.type + "</th>  <th>" + dateString + "</th></tr>");
                    })

                }


            }
        }
    })
}


function getTspBalance() {
    var publicKey = $("#wallet_adress").text();
    // var publicKey = "0xda4bb5e8dee99e2aba390372abf880653c774661";

    var dataToSend = {
        'address': publicKey,
    };

    console.log(dataToSend);
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: '/api/v1/tsp/tsp_wallet_web/',
        async: true,
        data: JSON.stringify(dataToSend),
        dataType: 'json',
        success: function (result) {
            if (result["status"] === "success") {
                var data = result['data'];
                console.log(data);
                var tspValue = data['tsp_value'];
                console.log("Tsp Value" + tspValue);
                $('#tsp_balance').text(tspValue + " TSP");
            }
        }
    })
}
var point=("#tsp_balance").text(tspValue+"TSP");
function getPoint(){
    console.log(point);
}

function sendTSP() {
    // var publicKey = $("#wallet_adress").text();
    var fromAddress = $("#wallet_adress").text();
    var toAddress = $("#toAddress").val();
    var tspValue = $("#tspAmount").val();
    var dataToSend = {
        'from_address': fromAddress,
        'to_address': toAddress,
        'tsp_value': tspValue
    };

    console.log(dataToSend);
    $.ajax({
        type: 'POST',
        contentType: 'application/json',
        url: '/api/v1/tsp/sendTSP_web/',
        async: true,
        data: JSON.stringify(dataToSend),
        dataType: 'json',
        success: function (result) {
            console.log(result);
            if (result["status"] === "success") {
                getTspBalance();
                $("#toAddress").val("");
                $("#tspAmount").val("");
                alert("Success");
            }
        }
    })
}
